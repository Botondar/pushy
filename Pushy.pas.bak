program Pushy;

uses windows, gl, math, sysutils;

(* TYPES *)
type
    FInterp = function(a, b, t : real) : real;

    vec2 = record
        x, y : real;
    end;

    vec3 = record
        x, y, z : real;
    end;

    TTimer = record
        startT, endT : LARGE_INTEGER;
        elapsedMillis : real;
    end;

    TKeyboard = Array[VK_LBUTTON..VK_OEM_CLEAR] of longint;

    TPlayerAnimation = record
        play : boolean;
        duration, elapsedTime : real;
        endSize, startSize : real;
        interp : FInterp;
    end;

    // TODO: add mass
    TPlayer = record
        x, y, dx, dy : real;
        size : real;
        color : vec3;
        lose : boolean;
        animation : TPlayerAnimation;
    end;

    FPowerUp = procedure(var p : TPlayer);
    TPowerUp = record
        Event : FPowerUp;
    end;

    TGameState = record
        keyboard : TKeyboard;
        p1, p2 : TPlayer;
        collide : boolean;
    end;

(* CONSTS *)
const
    SPEED = 0.0006;
    SLOW = 0.0002;
    DEFAULT_SIZE = 0.2;
    SQRT_2 = 1.41421356237;

(* GLOBALS *)
var
    gWindowClass : WNDCLASS;
    gScreenWidth, gScreenHeight : integer;
    gAspect : real;
    gFrequency : int64;
    gTimer : TTimer;

(* PROCS AND FUNCS *)
function WindowProc(Window : HWND; msg : UINT; wParam : WPARAM; lParam : LPARAM) : LRESULT; stdcall; export;
var
    Res : LRESULT;
begin
    Res := 0;
    case msg of
        WM_CLOSE, WM_DESTROY:
        begin
             DestroyWindow(Window);
             PostQuitMessage(0);
        end;
        WM_SIZE:
        begin
            gScreenWidth := LOWORD(lParam);
            gScreenHeight := HIWORD(lParam);
            gAspect := gScreenWidth / gScreenHeight;
            glViewport(0, 0, gScreenWidth, gScreenHeight);
            Res := DefWindowProc(Window, msg, wParam, lParam);
        end;
        else Res := DefWindowProc(Window, msg, wParam, lParam);
    end;
    WindowProc := Res;
end;

function CreateWindowClass() : boolean;
var
    Res : boolean;
begin
    Initialize(gWindowClass);
    gWindowClass.lpfnWndProc := @WindowProc;
    gWindowClass.Style := CS_OWNDC;
    gWindowClass.lpszClassName := 'Pascal';
    gWindowClass.hCursor := LoadCursor(GetModuleHandle(nil), IDC_ARROW);
    Res := RegisterClass(gWindowClass) <> 0;

    CreateWindowClass := Res;
end;

function CreateOGLContext(Window : HWND) : HDC;
var
    pfd : PIXELFORMATDESCRIPTOR;
    PixelFormat : integer;
    DeviceContext : HDC;
    RenderingContext : HGLRC;
begin
    Initialize(pfd);
    pfd.nVersion := 1;
    pfd.dwFlags := PFD_DRAW_TO_WINDOW OR PFD_SUPPORT_OPENGL OR PFD_DOUBLEBUFFER;
    pfd.iPixelType := PFD_TYPE_RGBA;
    pfd.cColorBits := 32;
    pfd.cDepthBits := 24;
    pfd.cStencilBits := 8;
    pfd.iLayerType := PFD_MAIN_PLANE;

    DeviceContext := GetDC(Window);

    PixelFormat := ChoosePixelFormat(DeviceContext, @pfd);
    SetPixelFormat(DeviceContext, PixelFormat, @pfd);

    RenderingContext := wglCreateContext(DeviceContext);
    wglMakeCurrent(DeviceContext, RenderingContext);

    CreateOGLContext := DeviceContext;
end;

function IsKeyDown(keyboard : TKeyboard; key : longint) : boolean;
begin
     IsKeyDown := (keyboard[key] AND $80000000) = 0;
end;

function WasKeyDown(keyboard : TKeyboard; key : longint) : boolean;
begin

    WasKeyDown := (Keyboard[key] AND $40000000) <> 0;
end;

procedure InitTimer();
var
    Frequency : LARGE_INTEGER;
begin
     QueryPerformanceFrequency(@Frequency);
     gFrequency := Frequency.QuadPart;
end;

function StartTimer(var t : TTimer) : int64;
begin
     QueryPerformanceCounter(@t.startT);
     StartTimer := t.startT.QuadPart;
end;

function EndTimer(var t : TTimer) : int64;
begin
     QueryPerformanceCounter(@t.endT);
     EndTimer := t.endT.QuadPart;
end;

function GetElapsedMillis(var t : TTimer) : real;
begin
     t.elapsedMillis :=  1000.0 * (t.endT.QuadPart - t.startT.QuadPart) / gFrequency;
     GetElapsedMillis := t.elapsedMillis;
end;

function lerp(a, b, t : real) : real;
begin
     lerp := a + t * (b - a);
end;

// quadratic interp
function qerp(a, b, t : real) : real;
begin
     qerp := a + t * t * (b - a);
end;

procedure RenderSquare(x, y, size : real);
begin
    glBegin(GL_TRIANGLES);
    glVertex3f(x + -size, y + -size, 0.0);
    glVertex3f(x +  size, y + -size, 0.0);
    glVertex3f(x +  size, y +  size, 0.0);
    glVertex3f(x + -size, y + -size, 0.0);
    glVertex3f(x +  size, y +  size, 0.0);
    glVertex3f(x + -size, y +  size, 0.0);
    glEnd();
end;

procedure RenderPlayer(p : TPlayer);
begin
    glColor3f(p.color.x, p.color.y, p.color.z);
    RenderSquare(p.x, p.y, p.size);
end;

procedure Render(DeviceContext : HDC; var state : TGameState);
begin
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(-gAspect, gAspect, -1.0, 1.0, -1.0, 1.0);

    RenderPlayer(state.p2);
    RenderPlayer(state.p1);

    SwapBuffers(DeviceContext);
end;

procedure StartAnimation(var p : TPlayer; duration, size : real);
begin
     p.animation.startSize := p.size;
     p.animation.endSize := size;
     p.animation.elapsedTime := 0.0;
     p.animation.duration := duration;
     p.animation.play := true;
end;

procedure TickAnimation(var p : TPlayer);
begin
    if p.animation.play then
    begin
         p.animation.elapsedTime += gTimer.elapsedMillis;
         if(p.animation.elapsedTime >= p.animation.duration) then
         begin
             p.animation.play := false;
             p.size := p.animation.endSize;
         end
         else
             p.size := p.animation.interp(p.animation.startSize, p.animation.endSize, p.animation.elapsedTime / p.animation.duration);
    end;
end;
// TODO: add mass
procedure PhysTick(var p : TPlayer);
begin
    if abs(p.dx) < SLOW then p.dx := 0
    else p.dx -= sign(p.dx) * SLOW;

    if abs(p.dy) < SLOW then p.dy := 0
    else p.dy -= sign(p.dy) * SLOW;

    p.x += p.dx * gTimer.elapsedMillis;
    p.y += p.dy * gTimer.elapsedMillis;
end;

procedure TickPlayer(var p : TPlayer);
begin
    PhysTick(p);
    TickAnimation(p);
end;

function CheckCollide(p1, p2 : TPlayer) : boolean;
var
    xCollide, yCollide : boolean;
    npos1, npos2 : vec2;
begin
    npos1.x := p1.x + p1.dx * gTimer.elapsedMillis;
    npos1.y := p1.y + p1.dy * gTimer.elapsedMillis;
    npos2.x := p2.x + p2.dx * gTimer.elapsedMillis;
    npos2.y := p2.y + p2.dy * gTimer.elapsedMillis;

    xCollide := ((npos1.x - p1.size) < (npos2.x + p2.size)) AND ((npos1.x + p1.size) > (npos2.x - p2.size));
    yCollide := ((npos1.y - p1.size) < (npos2.y + p2.size)) AND ((npos1.y + p1.size) > (npos2.y - p2.size));
    CheckCollide := xCollide AND yCollide;
end;

function CheckLose(var p : TPlayer) : boolean;
var
   xOut, yOut : boolean;
begin
     xOut := ((p.x + p.size) < -gAspect) OR ((p.x - p.size) > gAspect);
     yOut := ((p.y + (p.size)) < -1) OR ((p.y - (p.size)) > 1);
     p.lose := xOut OR yOut;
     CheckLose := p.lose;
end;

procedure HandleCollide(var p1, p2 : TPlayer);
var
    dxRes, dyRes : real;
    p1Mass, p2Mass : real;
begin
    p1Mass := p1.size * p1.size;
    p2Mass := p2.size * p2.size;
    dxRes := (p1Mass * p1.dx + p2Mass * p2.dx) / (p1Mass + p2Mass);
    dyRes := (p1Mass * p1.dy + p2Mass * p2.dy) / (p1Mass + p2Mass);

    p1.dx := dxRes;
    p2.dx := dxRes;
    p1.dy := dyRes;
    p2.dy := dyRes;

end;

procedure Tick(var state : TGameState);
var
    i : integer;
begin


    // Reset
    if IsKeyDown(state.keyboard, VK_RETURN) AND NOT WasKeyDown(state.keyboard, VK_RETURN) then
    begin
         state.p1.x := -1.0;
         state.p1.y := 0.0;
         state.p1.dx := 0.0;
         state.p1.dy := 0.0;

         state.p2.x := 1.0;
         state.p2.y := 0.0;
         state.p2.dx := 0.0;
         state.p2.dy := 0.0;
    end;

    if IsKeyDown(state.keyboard, LongInt(' ')) AND NOT WasKeyDown(state.keyboard, LongInt(' ')) then
    begin
         StartAnimation(state.p1, 1000.0, state.p1.size * 2.5);
         StartAnimation(state.p2, 1000.0, state.p2.size * 2.5);
    end;

    if IsKeyDown(state.keyboard, LongInt('B')) AND NOT WasKeyDown(state.keyboard, LongInt('B')) then
    begin
        StartAnimation(state.p1, 1000.0, state.p1.size / 2.5);
        StartAnimation(state.p2, 1000.0, state.p2.size / 2.5);
    end;

    // Player movement
    if IsKeyDown(state.keyboard, LongInt('A')) then
        state.p1.dx -= SPEED
    else if IsKeyDown(state.keyboard, LongInt('D')) then
        state.p1.dx += SPEED;

    if IsKeyDown(state.keyboard, LongInt('S')) then
        state.p1.dy -= SPEED
    else if IsKeyDown(state.keyboard, LongInt('W')) then
        state.p1.dy += SPEED;

    if IsKeyDown(state.keyboard, LongInt(VK_LEFT)) then
        state.p2.dx -= SPEED
    else if IsKeyDown(state.keyboard, VK_RIGHT) then
        state.p2.dx += SPEED;

    if IsKeyDown(state.keyboard, VK_DOWN) then
        state.p2.dy -= SPEED
    else if IsKeyDown(state.keyboard, VK_UP) then
        state.p2.dy += SPEED;

    // the OS only sends keydown messages infrequently
    // so we fix the 'was down' flag by hand
    for i := VK_LBUTTON to VK_OEM_CLEAR do
    begin
        if IsKeyDown(state.keyboard, i) then
            state.keyboard[i] := state.keyboard[i] OR $40000000;
    end;

    state.collide := CheckCollide(state.p1, state.p2);
    if state.collide then
    begin
         state.p2.color.z := 1.0;
         HandleCollide(state.p1, state.p2);
    end
    else
        state.p2.color.z := 0.0;

    TickPlayer(state.p1);
    TickPlayer(state.p2);

    CheckLose(state.p1);
    CheckLose(state.p2);

end;

(* MAIN *)
var
    (* API vars *)
    Window : HWND;
    DeviceContext : HDC;
    LoopMessage : MSG;
    Running : boolean;
    i : integer;

    (* Game vars *)
    state : TGameState;
    result : string;
BEGIN
    InitTimer;
    Initialize(gTimer);
    (* INITIALIZATION *)
    Initialize(state);
    for i := VK_LBUTTON to VK_OEM_CLEAR do
        state.keyboard[i] := $F0000000;

    state.p1.color.x := 1.0;
    state.p1.x := -1.0;
    state.p1.size := DEFAULT_SIZE;
    state.p1.animation.interp := @lerp;

    state.p2.color.y := 1.0;
    state.p2.x := 1.0;
    state.p2.size := DEFAULT_SIZE;
    state.p2.animation.interp := @qerp;

    state.collide := false;

    result := '';
    Randomize;

    if NOT CreateWindowClass then
        exit;

    Window := CreateWindow('Pascal', 'Pascal',
                           WS_OVERLAPPEDWINDOW,
                           CW_USEDEFAULT, CW_USEDEFAULT,
                           1280, 720,
                           0, 0, GetModuleHandle(nil), nil);
    if Window = 0 then
        exit;
    DeviceContext := CreateOGLContext(Window);

    ShowWindow(Window, SW_SHOW);

    glClearColor(0.0, 0.0, 0.0, 1.0);

    StartTimer(gTimer);
    (* MAIN LOOP *)
    Initialize(LoopMessage);
    Running := true;
    while Running do
    begin


        (* OS MESSAGES *)
        while PeekMessage(LoopMessage, 0, 0, 0, PM_REMOVE) do
        begin
            case LoopMessage.message of
                WM_QUIT:
                begin
                    Running := false;
                    break;
                end;
                WM_KEYDOWN, WM_KEYUP: state.keyboard[LoopMessage.wParam] := LoopMessage.lParam;
                else
                begin
                    TranslateMessage(LoopMessage);
                    DispatchMessage(LoopMessage);
                end;
            end;

        end;

        (* LOGIC + INPUT *)
        Tick(state);

        if state.p1.lose AND state.p2.lose then
             result := 'Nobody wins!'
        else if state.p1.lose then
             result := 'Player 2 wins!'
        else if state.p2.lose then
             result := 'Player 1 wins!';

        if state.p1.lose OR state.p2.lose then
           DestroyWindow(Window);

        (* RENDER *)
        Render(DeviceContext, state);

        (* TIMER *)
        EndTimer(gTimer);
        GetElapsedMillis(gTimer);
        //writeln(gTimer.elapsedMillis : 6 : 4, 'ms');
        StartTimer(gTimer);
    end;
    writeln(result);
    readln();
END.
